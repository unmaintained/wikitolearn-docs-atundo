# How to contribute

## Introduction
We are proudly part of [KDE Community](https://www.kde.org/). Our repositories are hosted on the [KDE Git infrastructure](https://cgit.kde.org/) and mirrored on GitHub at [KDE GitHub Mirror](https://github.com/KDE) and [WikiToLearn](https://github.com/WikiToLearn).

Our contributing workflow follows the KDE Community guidelines. Please, read [this guide](https://community.kde.org/Get_Involved) before to proceed.

## The WikiToLearn SDK
The [WikiToLearn SDK](https://cgit.kde.org/wikitolearn-sdk.git/about/) is a Python + Bash utility which helps developers to reproduce the entire WikiToLearn microservices stack locally.

The SDK is able to download the repositories, build-run-stop the services, and also to perform the restore of production-like data into the involved services' database.

!!! tip
    We suggest you to use the SDK if you want to execute the whole architecture or if you want to develop some components using its dependencies directly.
    For instance, if you need to develop a [backend](architecture.md#backend-layer) service, it usually does not need any other part of the architecture. Hence, you can avoid to use the SDK in that case.
