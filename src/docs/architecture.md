# Architecture

## Reasons
WikiToLearn architecture is a microservices architecture. We decided to drop the old fashioned monolithic architecture based on MediaWiki in favor of a cloud-native architecture.

This choice was lead by:

1. our insatiable will to learn and apply new technologies
2. our cloud infrastructure which is not exploited to the full due to a monolithic architecture
3. the difficulties in MediaWiki development and customization due to an outdated documentation and application design

Since the beginning, we developed from scratch or forked several MediaWiki extensions to provide cool and user-friendly features to our users. We completely re-defined the user journey and tried to tighten up the "too much open" features of a MediaWiki-based application.

After approximately one year of development, we agreed MediaWiki was not the right way to go. It powers one of the most important website over the Internet, but it cannot address all the features and the use cases we would like to implement.

Every time we wanted to fix a bug or implement a new feature was a mess. Developers learning curve was steep and the lack of modularity was a pain for a newbie.

Therefore, we decided to switch a custom solution starting from scratch. And obviously, we decided for a microservices architecture since we already embrace (or trying to...) some emerging ideas such as containerization and DevOps in other projects.

## Main concepts
An overview of the architecture is show in the image below:

![Architecture overview](img/architecture_overview.jpg)

*Fig 1: Architecture overview*

We will focus on the yellow, red and pink parts of the diagram:

* __Yellow__: Gateway layer
* __Red__: Mid-Tier layer
* __Pink__: Backend layer

### Gateway layer
The Gateway layer implements the [API Gateway](http://microservices.io/patterns/apigateway.html) pattern. It exposes coarse-grained APIs to the clients and it is designed with the [Backends-For-Frontends (BFF)](https://samnewman.io/patterns/architectural/bff/) design pattern on mind.

The gateway layer is responsible for proxying and routing requests to the Mid-Tier layer, performing authentication checks, enabling circuit breaking and in the future API rate-limiting. It is the only entrypoint for the whole application.

### Mid-Tier layer
The Mid-Tier layer acts as the first layer of services. Even if microservices architecture prefers choreography over orchestration, there are some non-trivial issues in managing and aggregating dozens of dependent requests to services. Also, event-driven approaches do not fit all the scenarios and create shared dependencies on message brokers between services.

Many organizations who embraces microservices, usually put requests aggregation and transformation within the API Gateway. We preferred to off-load the Gateway layer from these capabilities and put them within the Mid-Tier layer. The Mid-Tier layer abstracts away all backend services and dependencies behind facades. As a result, the clients accesses through the gateway to "functionality" rather than a "system". This orchestration layer may sound like an Enterprise Service Bus (ESB). However, splitting it into many functionality-oriented services we tried to mitigate the Single Point of Failure (SPOF) risk and the spaghetti code. We have to give credits to Netflix for inspiring us with [this blog post](https://medium.com/netflix-techblog/engineering-trade-offs-and-the-netflix-api-re-architecture-64f122b277dd).

Moreover, we decided to use message brokers and to implement event-driven functionalities at Mid-Tier layer. This, makes the Backend layer as dumb as possible and reduces shared dependencies at minimum.

### Backend layer
The Backend layer is composed by dumb microservices which (usually) expose a REST API and hold the business logic to manage a nailed functionality.

Each microservice manages its own data structures and it is designed to avoid shared dependencies. Services within the Backend layer cannot communicate directly to each other. This constraint allows to evolve and improve them independently. The changes in the API will impact the Mid-Tier layer services who use it but no one of the Backend layer.
Moreover, this constraint allows to develop a Backend layer microservice without any other dependency except for its own databases or queues.
